package main

import (
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

type Conns struct {
	sync.Mutex
	conns  []net.Conn
	wr, rd uint32
}

func (cs *Conns) Set(conn net.Conn) {
	cs.Lock()
	cs.conns[cs.wr&1023] = conn
	cs.wr++
	cs.Unlock()
}
func (cs *Conns) Get() (conn net.Conn) {
	cs.Lock()
	conn = cs.conns[cs.rd&1023]
	if conn != nil {
		cs.conns[cs.rd&1023] = nil
		cs.rd++
	}
	cs.Unlock()
	return
}
func main() {
	n := 1024
	conns := &Conns{sync.Mutex{}, make([]net.Conn, n), 0, 0}
	var z sync.RWMutex
	var f atomic.Pointer[func()]
	q := func() {}
	r := func() {
		f.Store(&q)
		z.Unlock()
	}
	rspn := []byte("HTTP/1.1 404\r\nContent-Length: 0\r\n\r\n")
	body := make([]byte, 1024)
	nw := "tcp"
	addr := ":8080"
	lstnr, err := net.Listen(nw, addr)
	if err != nil {
		log.Fatalln(err)
	}
	for i := 0; i < 9; i++ {
		go func(hndlr func(net.Conn)) {
			for {
				if z.TryLock() {
					f.Store(&r)
				}
				z.RLock()
				z.RUnlock()
				conn := conns.Get()
				if conn != nil {
					hndlr(conn)
				}
			}
		}(func(conn net.Conn) {
			n, err := conn.Read(body)
			if err != nil {
				log.Println(err)
				return
			}
			conn.Write(rspn)
			conn.Close()
			println(string(body[:n]))
		})
	}
	time.Sleep(time.Second)
	go func() {
		for {
			conn, err := lstnr.Accept()
			if err != nil {
				log.Println(err)
				time.Sleep(time.Second)
				continue
			}
			conns.Set(conn)
			(*f.Load())()
		}
	}()
	select {}
}
